# README #

* Based on official Python Alpine image
* Designed for easy use with Docker compose
* All modbus coid, discrete, holding & input registers are initialized to zero, and only the client can modify them.
* See pymodbus http://pymodbus.readthedocs.io/en/latest/examples/synchronous-client.html for code on clients
* Some sample code is checked into this repository.
* docker run -h alpine-modbus bosolsen/alpine-modbus -p 502 -n 2
    * where 502 is the host modbus port
    * where n = 2 is the number of modbus servers, each subsequent server will be on a an incremented port: 503, 504
* Can also run via ./run.bash -p 502 -n 2 which will also kill any existing modbus server docker containers
* Use the docker "--link" option if you need to access the ports from another container, other wise, you will have to use the docker "-p" option
