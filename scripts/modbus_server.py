#!/usr/bin/env python
'''
Pymodbus Server With Updating Thread
--------------------------------------------------------------------------

This is an example of having a background thread updating the
context while the server is operating. This can also be done with
a python thread::


'''
#---------------------------------------------------------------------------# 
# import the modbus libraries we need
#---------------------------------------------------------------------------# 
import argparse
#from pymodbus.server.async import StartTcpServer # Twisted
from pymodbus.server.sync import StartTcpServer # Native
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer
import signal
import sys
import time
from threading import Thread


#---------------------------------------------------------------------------# 
# configure the service logging
#---------------------------------------------------------------------------# 
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)

#---------------------------------------------------------------------------# 
# Register Constants (These mimmic CSUS modbus list)
#---------------------------------------------------------------------------# 
n_coil     = 2
n_discrete = 17
n_input    = 26
n_holding  = 1
interval   = 4

#---------------------------------------------------------------------------# 
# util functions
#---------------------------------------------------------------------------# 
def fib(n):
    return (n in (0,1) and [n] or [fib(n-1) + fib(n-2)])[0]

def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)

#---------------------------------------------------------------------------# 
# update thread helper
#---------------------------------------------------------------------------# 
def modbus_function_helper(server_id, context, fxn_code, address, count): 
    ''' Updates the context with new values. If coil/discrete, just flips the bits.
    If input/holding, we add one to the previous value
    '''

    values   = context.getValues(fxn_code, address, count=count)

    log.info("\tServer %s: Old values: %s" % (server_id, str(values)))

    # Simply Toggle Bit
    if fxn_code == 1 or fxn_code == 2:
        values   = [(v + 1) % 2 for v in values]
    # Simply Add Unity
    elif fxn_code == 3 or fxn_code == 4:
        values   = [v + 1 for v in values]

    log.info("\tServer %s: New values: %s" % (server_id, str(values)))
    context.setValues(fxn_code, address, values)

#---------------------------------------------------------------------------# 
# define register update thread
#---------------------------------------------------------------------------# 
def updating_writer(server_id, a):
    ''' A worker process that runs every so often and
    updates live values of the context. It should be noted
    that there is a race condition for the update.
    '''

    while True:
	context  = a[0]

	log.info("Updating context of Coils Server %s" % server_id)
        modbus_function_helper(server_id, context, fxn_code=1, address=0, count=n_coil)

	log.info("Updating context of Discrete Server %s" % server_id)
        modbus_function_helper(server_id, context, fxn_code=2, address=0, count=n_discrete)

	log.info("Updating context of Holding Server %s" % server_id)
        modbus_function_helper(server_id, context, fxn_code=3, address=0, count=n_holding)

	log.info("Updating context of Input Server %s" % server_id)
        modbus_function_helper(server_id, context, fxn_code=4, address=0, count=n_input)

	time.sleep(interval)

def server(server_id, context, address, port):
    ''' A worker which starts a modbus server
    '''
    
    log.debug("Starting modbus server %s at %s:%s" % (server_id, address, port))
    StartTcpServer(context, address=(address, port))

#---------------------------------------------------------------------------# 
# Main
#---------------------------------------------------------------------------# 
if __name__ == '__main__':

    #---------------------------------------------------------------------------# 
    # parse args
    #---------------------------------------------------------------------------# 
    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--host', type=str, default='localhost', help='Host')
    parser.add_argument('-p', '--port', type=int, default=55022, help='TCP port')
    parser.add_argument('-n', '--nserver', type=int, default=1, help='Number of servers')
    args = parser.parse_args()

    port = args.port

    #---------------------------------------------------------------------------# 
    # loop over number of servers
    #---------------------------------------------------------------------------# 
    for i in xrange(args.nserver):

        #---------------------------------------------------------------------------# 
        # initialize your data store
        #---------------------------------------------------------------------------# 
        store = ModbusSlaveContext(
            di = ModbusSequentialDataBlock(0, [0]*100),
            co = ModbusSequentialDataBlock(0, [1]*100),
            hr = ModbusSequentialDataBlock(0, [66]*100),
            ir = ModbusSequentialDataBlock(0, [666]*100))
        context = ModbusServerContext(slaves=store, single=True)

        #---------------------------------------------------------------------------# 
        # run the update thread
        #---------------------------------------------------------------------------# 
        update_thread = Thread(target=updating_writer, args=(i, context,))
        update_thread.daemon = True
        update_thread.start()

        #---------------------------------------------------------------------------# 
        # run the server
        #---------------------------------------------------------------------------# 
        server_thread = Thread(target=server, args=(i, context, args.host, port,))
        server_thread.daemon = True
        server_thread.start()

        port += 1

    #---------------------------------------------------------------------------# 
    # Waiting for signal
    #---------------------------------------------------------------------------# 
    signal.signal(signal.SIGINT, signal_handler)
    signal.pause()
