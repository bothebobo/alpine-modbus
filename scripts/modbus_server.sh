#!/bin/sh

# Simple wrapper for the python script 
# to automatically set the host ip

set -e

# Get Host IP
IP=`getent hosts $HOSTNAME | awk '{print $1}'`

# Run the modbus server
CMD="/modbus/modbus_server.py -H $IP $@"
echo "$CMD" && exec $CMD
