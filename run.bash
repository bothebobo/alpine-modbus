#!/bin/bash

# docker ancestor name
DOCKER_NAME=bosolsen/alpine-modbus

# docker host name
DOCKER_HOSTNAME=alpine-modbus

DOCKER_ATT="-h $DOCKER_HOSTNAME"

echo "KILLING PREVIOUS CONTAINERS"
RUNNING_CONTAINERS=$(docker ps -q -f ancestor=$DOCKER_NAME)
if [ ! -z $RUNNING_CONTAINERS ] ; then
  docker kill $RUNNING_CONTAINERS
fi

echo "STARTING CONTAINER"
CMD="docker run --rm $DOCKER_ATT $DOCKER_ENV $DOCKER_NAME $@" 
echo "$CMD" && $CMD
